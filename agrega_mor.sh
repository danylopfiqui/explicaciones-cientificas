#!/bin/bash

cd originales

for f in *.cha
do
	echo $f 
  
  NUMOFLINES=$(grep -c '' $f)

  if [ $NUMOFLINES -eq 10 ]
  then
  	awk 'NR==10{$0="%mor:\t\n%com:\t\n*EST:\t\n"$0}1' $f > ../modificados_cha/$f
  else
  	awk 'NR==10{$0="%mor:\t\n%com:\t\n"$0}1' $f > ../modificados_cha/$f
  fi
done
